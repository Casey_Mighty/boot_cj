macro package boot_cj.macors

import std.ast.*
import std.collection.ArrayList
import boot_cj.tools.ArrayListEnhancer
import boot_cj.utils. MacroUtils

/**
 * Class member descriptor which can derive public property for all non-public members
 * Attension: the first letter of the member name should be `_`, that means it needs to be derived
 */
public macro Descriptor(input: Tokens): Tokens {
    let decl = parseDecl(input)
    match(decl) {
        case clzDecl: ClassDecl => clzDecl.body.decls.appendAll(generateDescriptor(clzDecl.body.decls))
        case structDecl: StructDecl => structDecl.body.decls.appendAll(generateDescriptor(structDecl.body.decls))
        case _ =>throw Exception('Macro @Descriptor only can be used to evaluaton the declaration of class or struct.')
    }
    decl.toTokens()
}

func generateDescriptor(decls: ArrayList<Decl>): ArrayList<Decl> {
    let propDeclList = decls.filter({el => el is PropDecl})
    let propList = ArrayList<Decl>()
    for (decl in decls) {
        let varDeclOpt = decl as VarDecl
        if (varDeclOpt.isNone()) {
            continue
        }
        let varDecl = varDeclOpt.getOrThrow()
        let varName = varDecl.identifier.value
        if (!varName.startsWith('_')) {
            continue
        }
        if (MacroUtils.hasSomeModifier(varDecl, PUBLIC)) {
            continue
        }
        if (propDeclList.any({el => el.identifier.value == varName[1..]})[0]) {
            continue
        }
        var prefixToken = quote(public)
        var descriptorToken = quote(
            get() {
                return $(varDecl.identifier)
            }
        )
        if (MacroUtils.isMutable(varDecl)) {
            descriptorToken += quote(
                set(el) {
                    this.$(varDecl.identifier) = el
                }
            )
            prefixToken += quote( mut )
        }
        let propName = Token(IDENTIFIER, varName[1..])
        let property = PropDecl(prefixToken + quote(prop $(propName): $(varDecl.declType) {
            $(descriptorToken)
        }))
        propList.append(property)
    }
    propList
}